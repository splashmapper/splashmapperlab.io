Windows package
===============

`DISCLAIMER`: Windows support is still very early, and many issues have probably not be detected and fixed yet. If you feel like you found an issue, please report it on the `Splash issues tracker <https://gitlab.com/groups/splashmapper/-/issues>`__.

Download the package
~~~~~~~~~~~~~~~~~~~~

The latest Windows package can be downloaded at the following link: `Splash installation package for Windows <https://gitlab.com/splashmapper/splash/-/jobs/artifacts/master/download?job=package:windows>`__.

Package installation
~~~~~~~~~~~~~~~~~~~~

The package should have been downloaded to your `Download` directory. There you should find a file named `splash_windows_installer.zip`. Right click on it to uncompress it, then double click the extracted `splash-$VERSION-win64.exe` (with `$VERSION` the version of Splash you downloaded), and go through the installation process. You will have to accept the licence which is the standard `GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>`__.

Once installed, Splash should appear in the `Start menu`, or by looking for it in the search bar.

Package removal
~~~~~~~~~~~~~~~

To uninstall Splash, you just have to go through the same process as any other software.
