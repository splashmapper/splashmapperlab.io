Contact
=======

This free, libre open source software is developed by the `Lab148 cooperative <https://lab148.xyz>`__ team and the `[SAT]Metalab <https://sat.qc.ca/fr/recherche/metalab>`__.


Email
-----

Get in touch by email :

- `contact@splashmapper.xyz <mailto:contact@splashmapper.xyz>`__
