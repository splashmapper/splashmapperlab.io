Community
=========

If you want to share your projects done with Splash, ask questions, and help other users, the `forum <https://community.splashmapper.xyz>`__ is good place.

Get in touch with us by `asking questions on the issue tracker <https://gitlab.com/splashmapper/splash/-/issues>`__ or through the #splashmapper channel on `Matrix <https://matrix.to/#/#splashmapper:matrix.org>`__.
