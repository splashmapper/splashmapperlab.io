Video overlay
=============

Objects can have multiple images (or videos) linked to them. As of yet
there is no blending options, but if the second image has an alpha
channel it will be used as a mask to merge the two sources. The alpha
channel of the first image is completely discarded.

For still images, it is advised to use the PNG file format. For videos
the Hap Alpha codec is the best choice, converted from a sequence of PNG
files with transparency. See the `FAQ <#preferred-codecs>`__ for
information about how to convert from a PNG sequence to a Hap video
file.

To connect an additional image to an object, on the GUI control panel:

-  create a new image / image_ffmpeg / image_shmdata
-  select the file to play by setting its path in the image attributes
-  shift+click on the target object to link the image to it
