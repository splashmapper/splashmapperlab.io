SMPTE master clock
==================

Playback rate can be controlled through a master clock, following the
`LTC encoding <https://en.wikipedia.org/wiki/Linear_timecode>`__. The
timecode is received through an audio input, which by default is the
default audio input of the computer (basically, the line in or the
microphone input). The timecode is an audio file which can be generated
by a number of softwares, among which `Ardour <https://ardour.org/>`__
and the command line utility `ltcgen <https://github.com/x42/libltc>`__.

Using ltcgen (installed from the sources or from a package), one can
create an audio file containing the timecode with the following comand:

.. code:: bash

   # Generate a 1 minute timecode
   ltcgen -t 00:00 -l 00:01:00:00 /tmp/timecode.wav

This timecode can be played from another computer which output is
connected to the input of the Splash computer. It can also be played
locally using the Jack audio server. Ensure that Jack and mplayer are
installed:

.. code:: bash

   sudo apt install jackd mplayer

The following commands will run the jackd server, and run mplayer with
the previously generated audio file:

.. code:: bash

   jackd -R -d alsa &
   mplayer -ao jack:name=mplayer /tmp/timecode.wav -loop 0

Lastly, Splash must be set to read the timecode from Jack instead of the
default audio input. To do so, you have to set the
``Audio input device`` option from the ``Main`` GUI tabulation, and set
it to ``mplayer`` (which is the name we give to the Jack source in the
previous command line).

.. figure:: ../_static/images/set_input_clock_device.jpg
   :alt: Set input clock device

   Set input clock device

Splash should now receive the timecode from mplayer. As a command line
tool, mplayer can be paused with the space bar, and seeked with the
arrow keys. In Splash, the current time code can be seen in the
``Timings`` panel of the GUI.

To terminate the Jack server, type the following command:

.. code:: bash

   killall jackd
