Set output audio device for videos
==================================

Splash has the capability of reading the audio from video files, even
though it is usually read from another software/computer and
synchronized with Splash through an SMPTE clock (see previous section).
This is done individually for each video media, which can give rise to
interesting setups using multiple audio output devices.

In the ``Medias`` GUI tabulation, given that the selected media is of
type ``video``, there will be an attribute named ``audioDeviceOutput``
which can be set to the name of any audio output device. As Splash uses
`PortAudio <http://portaudio.com/>`__ it can access devices through a
lot of API, i.e. PulseAudio or Jack.

.. figure:: ../_static/images/audio_output_image_ffmpeg.jpg
   :alt: Set audio output device

   Set audio output device

For Jack a list of the available devices is available through the
following command:

.. code:: bash

   jack_lsp
